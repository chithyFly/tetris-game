#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LENGTH 100

// 声明排序函数
void sort(char* str1, char* str2, char* result);

int main() {
    // 打开文件a1.txt和b1.txt
    FILE* file1 = fopen("a1.txt", "r");
    FILE* file2 = fopen("b1.txt", "r");

    if (file1 == NULL || file2 == NULL) {
        printf("Failed to open file.\n");
        exit(1);
    }

    // 读取文件a1.txt和b1.txt中的字符
    char str1[MAX_LENGTH];
    fgets(str1, MAX_LENGTH, file1);
    char str2[MAX_LENGTH];
    fgets(str2, MAX_LENGTH, file2);

    // 关闭文件a1.txt和b1.txt
    fclose(file1);
    fclose(file2);

    // 创建新文件c1.txt并打开
    FILE* file3 = fopen("c1.txt", "w");

    if (file3 == NULL) {
        printf("Failed to create file.\n");
        exit(1);
    }

    // 调用排序函数，将排序后的结果写入文件c1.txt
    char result[MAX_LENGTH * 2];
    sort(str1, str2, result);
    fputs(result, file3);

    // 关闭文件c1.txt
    fclose(file3);

    return 0;
}

// 实现排序函数
void sort(char* str1, char* str2, char* result) {
    char temp[MAX_LENGTH * 2];
    strcpy(temp, str1);
    strcat(temp, str2);
    int len = strlen(temp);

    for (int i = 0; i < len - 1; i++) {
        for (int j = 0; j < len - i - 1; j++) {
            if (temp[j] > temp[j + 1]) {
                char tempChar = temp[j];
                temp[j] = temp[j + 1];
                temp[j + 1] = tempChar;
            }
        }
    }

    strcpy(result, temp);
}
