#pragma warning(disable:4996)
#include <cstdio>
#include <time.h>
#include <conio.h>
#include <stdlib.h>
#include <windows.h>
#include <mmsystem.h>
#include <graphics.h>
#pragma comment(lib,"Winmm.lib")

#include "TetrisGame.h"

// 定义游戏开始时间
extern DWORD   start_time = 0;
// 定义游戏计时器
extern DWORD timer = 0;

// 定义返回按钮坐标常量
extern const int backTextX1 = 10;
extern const int backTextY1 = 10;
extern int backTextX2 = 0;
extern int backTextY2 = 0;

// 定义暂停游戏按钮的坐标常量
extern const int pauseTextX1 = 10 * StepSize + 10;
extern const int pauseTextY1 = 6 * StepSize;
extern int pauseTextX2 = 0;
extern int pauseTextY2 = 0;

// 定义暂停游戏界面中“重新开始游戏”按钮的坐标常量
extern const int restartTextX1 = 10 * StepSize + 10;
extern const int restartTextY1 = 7 * StepSize + 10;
extern int restartTextX2 = 0;
extern int restartTextY2 = 0;

// 定义暂停游戏界面中“返回欢迎界面”按钮的坐标常量
extern const int returnTextX1 = 10 * StepSize + 10;
extern const int returnTextY1 = 8 * StepSize + 20;
extern int returnTextX2 = 0;
extern int returnTextY2 = 0;


char AllBlock[20][10] = { 0 };
char Block[6][4] = {
	{ 0, 1, 5, 6 },
	{ 4, 5, 6, 7 },
	{ 1, 4, 5, 6 },
	{ 0, 4, 5, 6 },
	{ 1, 2, 5, 6 },
	{ 1, 2, 4, 5 }
};

unsigned char CurrBlo[4];
unsigned char Buff[4];
unsigned short Score = 0;
unsigned short targetScore = 20;
int Delay = 300;

char CurrSty = 0;
char NextSty = 0;
char DirX = 0;

bool RefreSign = false;
bool IsRun = true;
bool LongPresAble = false;

TCHAR currentScoreInfo[15];
TCHAR targetScoreInfo[15];

int difficultBtnX2 = 0;
int middleBtnX2 = 0;
int simpleBtnX2 = 0;

COORD RotaOrig = { 4, 1 };
IMAGE ImgBlo[6];
IMAGE ImgTem;


ExMessage msg;

// 定义不同的界面类型
enum UIType {
	WELCOME_UI,
	GAME_UI,
	GAME_SETTINGS_UI,
	HELP_UI
};

UIType currentUI = WELCOME_UI;

// 游戏主函数
void runGame() {
	// 显示欢迎界面
	currentUI = WELCOME_UI;
	showWelcomeUI();
	// 设置背景映音乐
	PlaySound(TEXT("Panda.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
	// 检测鼠标事件
	// 初始化为欢迎界面
	while (true) {
		// 处理鼠标消息
		MOUSEMSG msg = GetMouseMsg();
		if (msg.uMsg == WM_LBUTTONDOWN) {
			if (currentUI == WELCOME_UI) {
				// 在欢迎界面处理用户的点击事件
				if (msg.x >= 200 && msg.x <= 400 && msg.y >= 300 && msg.y <= 350) {
					// 点击开始游戏按钮
					EndBatchDraw();
					cleardevice();
					playGame();
					currentUI = GAME_UI;
				}
				else if (msg.x >= 200 && msg.x <= 400 && msg.y >= 400 && msg.y <= 450) {
					// 点击游戏设置按钮
					EndBatchDraw();
					cleardevice();
					showGameSettingsUI();
					currentUI = GAME_SETTINGS_UI;

				}
				else if (msg.x >= 200 && msg.x <= 400 && msg.y >= 500 && msg.y <= 550) {
					// 点击帮助按钮
					cleardevice();
					showHelpUI();
					currentUI = HELP_UI;
				}
				else if (msg.x >= 200 && msg.x <= 400 && msg.y >= 600 && msg.y <= 650) {
					// 点击退出按钮	
					EndBatchDraw();
					cleardevice();
					exit(0);
				}
			}
			else if (currentUI == GAME_SETTINGS_UI) {
				if (msg.x >= backTextX1 - 5 && msg.x <= backTextX2 + 5 && msg.y >= backTextY1 - 5 && msg.y <= backTextY2 + 5) {
				// 点击返回按钮
					EndBatchDraw();
					cleardevice();
					showWelcomeUI();
					currentUI = WELCOME_UI;	
				}
				else if (msg.x >= 200 && msg.x <= difficultBtnX2 && msg.y >= 200 && msg.y <= 250) {
					if (MessageBox(NULL, "是否要设置目标分数为100分？", "提示", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						// 用户点击了“确定”按钮
						targetScore = 100;
					}
				}
				else if (msg.x >= 200 && msg.x <= middleBtnX2 && msg.y >= 300 && msg.y <= 350) {
					// 点击困难按钮
					if (MessageBox(NULL, "是否要设置目标分数为50分？", "提示", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						targetScore = 50;
					}
				}
				else if (msg.x >= 200 && msg.x <= simpleBtnX2 && msg.y >= 400 && msg.y <= 450) {
					// 点击返回按钮
					if (MessageBox(NULL, "是否要设置目标分数为20分？", "提示", MB_YESNO | MB_ICONQUESTION) == IDYES) {
						targetScore = 20;
					}
				} else if (currentUI == GAME_SETTINGS_UI) {}
			}
			else if (currentUI == HELP_UI) {
								// 在帮助界面处理用户的点击事件
				if (msg.x >= backTextX1 - 5 && msg.x <= backTextX2 + 5 && msg.y >= backTextY1 - 5 && msg.y <= backTextY2 + 5) {
					// 点击返回按钮
					EndBatchDraw();
					cleardevice();
					showWelcomeUI();
					currentUI = WELCOME_UI;
				}
			}
		}
	}
}

// 显示欢迎界面
void showWelcomeUI() {
	// 绘制欢迎界面
	srand((unsigned int)time(NULL));
	// 获取左屏幕的分辨率
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);
	int leftScreenWidth = screenWidth / 2;

	// 计算图形窗口在左屏幕中央的位置
	int windowWidth = 700;  // 窗口宽度
	int windowHeight = 800;  // 窗口高度
	int windowX = (leftScreenWidth - windowWidth) / 2;  // 窗口左上角 x 坐标
	int windowY = (screenHeight - windowHeight) / 2;  // 窗口左上角 y 坐标

	// 创建图形窗口，并将其设置到左屏幕中间
	initgraph(windowWidth, windowHeight);
	HWND hWnd = GetHWnd();
	SetWindowPos(hWnd, NULL, windowX, windowY, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	SetWindowText(hWnd, _T("俄罗斯方块"));
	setbkcolor(WHITE);
	cleardevice();
	setlinecolor(BLACK);
	cleardevice();
	settextcolor(BLACK);
	settextstyle(40, 0, _T("宋体"));
	outtextxy(150, 100, _T("欢迎进入俄罗斯方块游戏"));

	// 绘制按钮1
	rectangle(200, 300, 400, 350);
	settextcolor(BLACK);
	settextstyle(20, 0, _T("宋体"));
	outtextxy(250, 310, _T("开始游戏"));

	// 绘制按钮2
	rectangle(200, 400, 400, 450);
	settextcolor(BLACK);
	settextstyle(20, 0, _T("宋体"));
	outtextxy(250, 410, _T("游戏设置"));


	// 绘制按钮3
	rectangle(200, 500, 400, 550);
	settextcolor(BLACK);
	settextstyle(20, 0, _T("宋体"));
	outtextxy(250, 510, _T("操作说明"));
	// 绘制按钮4
	rectangle(200, 600, 400, 650);
	settextcolor(BLACK);
	settextstyle(20, 0, _T("宋体"));
	outtextxy(250, 610, _T("退出游戏"));

}

// 开始游戏
void playGame() {
	DWORD gameStartTime = 0;   // 记录游戏开始的时间
	DWORD lastUpdateTime = 0;  // 记录上次更新计时器的时间
	char Count = -2;
	clock_t BegT = clock();

	// 设置游戏变量
	showGameUI();
	BeginBatchDraw();

	// 初始化游戏时间和计时器
	gameStartTime = GetTickCount();
	lastUpdateTime = gameStartTime;
	bool isGameStarted = false;
	bool isPKeyDown = false;
	bool isPaused = false;
	DWORD pauseStartTime = 0; // 新增变量，记录游戏暂停的开始时间
	DWORD pauseEndTime = 0; // 新增变量，记录游戏暂停的结束时间
	DWORD pauseDuration = 0; // 新增变量，记录游戏暂停的时长
	timer = 0;

	//等待空格按下 空格按下开始游戏
	settextstyle(20, 0, _T("宋体"));
	settextcolor(BLACK);
	outtextxy(1 * StepSize, 10 * StepSize, _T("按下空格 开始游戏"));
	outtextxy(pauseTextX1, 1 * StepSize, _T("下一个方块: "));
	ProdBlock();
	do {
		getmessage(&msg, EM_KEY);
		
	} while (msg.vkcode != VK_SPACE);
	do {
		showScoreInfo();
		Refresh();

		//游戏循环
		while (IsRun) {
			processMessage();
			// 暂停游戏


			// 新增代码，检查是否按下Space键
			if (GetAsyncKeyState(VK_SPACE) & 0x8000) {
				if (!isPKeyDown) { // 新增代码，检查是否是第一次按下Space
					if (!isGameStarted) { // 新增代码，如果游戏还没有开始，那么就开始游戏
						isGameStarted = true;
					}
					else { // 新增代码，处理游戏暂停逻辑
						isPaused = !isPaused;
						if (isPaused) {
							pauseStartTime = GetTickCount(); // 记录游戏暂停的开始时间
						}
						else {
							clearrectangle(1 * StepSize, 9 * StepSize, 6 * StepSize, StepSize);
							pauseEndTime = GetTickCount(); // 记录游戏暂停的结束时间
							pauseDuration += pauseEndTime - pauseStartTime; // 累加游戏暂停的时长
							isPaused = false; // 新增代码，自动继续游戏
						}
					}
					isPKeyDown = true; // 新增代码，标记 P 键已经按下
				}
			}
			else {
				isPKeyDown = false; // 新增代码，标记 P 键已经释放
			}
			if (GetAsyncKeyState('R') & 0x8000 || GetAsyncKeyState('r') & 0x8000) {
				// 显示提示框，询问是否重新开始
				int result = MessageBox(NULL, _T("是否返回主菜单？"), _T("提示"), MB_YESNO | MB_ICONQUESTION);
				if (result == IDYES) {
					clearAllBlock();
					// 关闭图形界面
					EndBatchDraw();
					closegraph();
					// 显示游戏界面
					playGame();
				} 
			}
			// 新增代码，检查是否按下 Esc 键
			if (GetAsyncKeyState(VK_ESCAPE) & 0x8000) {
				// 显示提示框，询问是否返回主菜单
				int result = MessageBox(NULL, _T("是否返回主菜单？"), _T("提示"), MB_YESNO | MB_ICONQUESTION);
				if (result == IDYES) {
					// 返回主菜单
					currentUI = WELCOME_UI;
					clearAllBlock();
					EndBatchDraw();
					closegraph();
					runGame();
					exit(0);
				}
			}

			// 如果游戏暂停，跳过本次循环
			if (isPaused) {
				Sleep(1);
				continue;
			}
			// 更新计时器
			DWORD currentTime = GetTickCount();
			if (currentTime - lastUpdateTime >= 1000) {
				timer = (currentTime - gameStartTime - pauseDuration) / 1000; // 减去游戏暂停的时长
				showTimerInfo(timer);
				lastUpdateTime = currentTime;
			}


			if (currentTime - BegT > Delay) {  // 使用 BegT 变量
				Drop();
				RefreSign = true;
				if (Delay == 68) Count++;
				if (Count >= 1) {
					Delay = 298;
					Count = -2;
				}
				BegT = currentTime;
			}

			if (DirX != -2) MoveX();
			if (RefreSign) Refresh();

			// 用 Sleep(1) 而不是 Sleep(3)
			Sleep(1);
		}

		// 游戏结束提示
		int choice = 0;
		choice = MessageBox(NULL, _T("游戏结束，您想重新开始还是返回主菜单？"), _T("游戏结束"), MB_OKCANCEL | MB_ICONQUESTION);
		if (choice == IDOK) {
			// 重新开始游戏
			clearAllBlock();
			IsRun = true;
			EndBatchDraw();
			closegraph();
			playGame();
		}
		else {
			IsRun = true;
			currentUI = WELCOME_UI;
			EndBatchDraw();
			clearAllBlock();
			closegraph();
			runGame();
			exit(0);
		}
	} while (true);
	EndBatchDraw();
	closegraph();
}

// 显示游戏界面
void showGameUI()
{
	// 获取左屏幕的分辨率
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);
	int leftScreenWidth = screenWidth / 2;

	// 计算图形窗口在左屏幕中央的位置
	int windowWidth = 500;  // 窗口宽度
	int windowHeight = 600;  // 窗口高度
	int windowX = (leftScreenWidth - windowWidth) / 2;  // 窗口左上角 x 坐标
	int windowY = (screenHeight - windowHeight) / 2;  // 窗口左上角 y 坐标

	// 创建图形窗口，并将其设置到左屏幕中间
	initgraph(windowWidth, windowHeight);
	HWND hWnd = GetHWnd();
	SetWindowPos(hWnd, NULL, windowX, windowY, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	IMAGE background;
	loadimage(&background, _T("background.png"));  // 背景图片的路径

	putimage(0, 0, &background);
	srand((unsigned int)time(NULL));
	HWND hwnd = GetHWnd();
	SetWindowText(hwnd, _T("俄罗斯方块"));
	setbkcolor(WHITE);
	cleardevice();
	setlinecolor(BLACK);
	line(301, 0, 301, 600);
	loadimage(&ImgTem, _T("background.png"), 6 * StepSize, StepSize);
	SetWorkingImage(&ImgTem);

	for (int i = 0; i < 6; i++)
		getimage(&ImgBlo[i], StepSize * i, 0, StepSize, StepSize);
	SetWorkingImage();


	// 绘制Pause按钮
	settextstyle(20, 0, _T("宋体"));
	setcolor(BLACK);
	const TCHAR* text = _T("暂停游戏");
	int textWidth = textwidth(text);
	int textHeight = textheight(text);
	outtextxy(pauseTextX1, pauseTextY1, text);
	pauseTextX2 = pauseTextX1 + textWidth;
	pauseTextY2 = pauseTextY1 + textHeight;
	rectangle(pauseTextX1 - 5, pauseTextY1 - 5, pauseTextX2 + 5, pauseTextY2 + 5);

	// 绘制Restart按钮
	settextstyle(20, 0, _T("宋体"));
	setcolor(BLACK);
	text = _T("重新开始");
	textWidth = textwidth(text);
	textHeight = textheight(text);
	outtextxy(restartTextX1, restartTextY1, text);
	restartTextX2 = restartTextX1 + textWidth;
	restartTextY2 = restartTextY1 + textHeight;
	rectangle(restartTextX1 - 5, restartTextY1 - 5, restartTextX2 + 5, restartTextY2 + 5);

	settextstyle(20, 0, _T("宋体"));
	setcolor(BLACK);
	text = _T("返回主菜单");
	textWidth = textwidth(text);
	textHeight = textheight(text);
	outtextxy(returnTextX1, returnTextY1, text);
	returnTextX2 = returnTextX1 + textWidth;
	returnTextY2 = returnTextY1 + textHeight;
	rectangle(returnTextX1 - 5, returnTextY1 - 5, returnTextX2 + 5, returnTextY2 + 5);

	_stprintf_s(targetScoreInfo, _T("目标得分: % 4d"), targetScore);
	outtextxy(pauseTextX1, 10 * StepSize, targetScoreInfo);

	Score = 0;
	showScoreInfo();
	showTimerInfo(timer / 1000);
}


void drawBackground() {
	IMAGE background;
	loadimage(&background, _T("background.png"));  // 背景图片的路径

	putimage(0, 0, &background);
}

void drawButtonAndText(const TCHAR* text, int x1, int y1, int height) {
	// 计算文本的宽度和高度
	int textWidth = textwidth(text);
	int textHeight = textheight(text);
	
	// 计算按钮的宽度
	int buttonWidth = textWidth + height;

	// 计算按钮的右下角坐标
	int x2 = x1 + buttonWidth;
	int y2 = y1 + height;

	// 计算文本在按钮中心的位置
	int textX = x1 + (buttonWidth - textWidth) / 2;
	int textY = y1 + (height - textHeight) / 2;

	// 绘制按钮和文本
	setcolor(BLACK);
	rectangle(x1, y1, x2, y2);
	outtextxy(textX, textY, text);
}

void showGameSettingsUI() {
	// 绘制按钮:困难
	drawButtonAndText(_T("困难"), 200, 200, 50);
	difficultBtnX2 = 200 + 50;
	// 绘制按钮: 中等
	drawButtonAndText(_T("中等"), 200, 300, 50);
	middleBtnX2 = 300 + 50;

	// 绘制按钮: 简单
	drawButtonAndText(_T("简单"), 200, 400, 50);
	simpleBtnX2 = 400 + 50;
	// 绘制Pause按钮
	settextstyle(20, 0, _T("宋体"));
	setcolor(BLACK);
	const TCHAR* text = _T("返回");
	int textWidth = textwidth(text);
	int textHeight = textheight(text);
	outtextxy(backTextX1, backTextY1, text);
	backTextX2 = backTextX1 + textWidth;
	backTextY2 = backTextY1 + textHeight;
	rectangle(backTextX1 - 5, backTextY1 - 5, backTextX2 + 5, backTextY2 + 5);
}
// 显示操作说明界面
void showHelpUI() {
	srand((unsigned int)time(NULL));

	// 获取左屏幕的分辨率
	int screenWidth = GetSystemMetrics(SM_CXSCREEN);
	int screenHeight = GetSystemMetrics(SM_CYSCREEN);
	int leftScreenWidth = screenWidth / 2;

	// 计算图形窗口在左屏幕中央的位置
	int windowWidth = 700;  // 窗口宽度
	int windowHeight = 800;  // 窗口高度
	int windowX = (leftScreenWidth - windowWidth) / 2;  // 窗口左上角 x 坐标
	int windowY = (screenHeight - windowHeight) / 2;  // 窗口左上角 y 坐标

	// 创建图形窗口，并将其设置到左屏幕中间
	initgraph(windowWidth, windowHeight);
	HWND hWnd = GetHWnd();
	SetWindowPos(hWnd, NULL, windowX, windowY, 0, 0, SWP_NOSIZE | SWP_NOZORDER);

	HWND hwnd = GetHWnd();
	SetWindowText(hwnd, _T("俄罗斯方块"));
	setbkcolor(WHITE);
	cleardevice();
	setlinecolor(BLACK);
	settextcolor(BLACK);
	settextstyle(20, 0, _T("宋体"));
	outtextxy(100, 50, _T("键盘操作: "));
	outtextxy(100, 100, _T("方向左键->方块左移一格"));
	outtextxy(100, 150, _T("方向右键->方块右移一格"));
	outtextxy(100, 200, _T("方向上键->方块旋转"));
	outtextxy(100, 250, _T("方向下键->方块加速下落"));
	outtextxy(100, 300, _T("空格键->暂停游戏"));
	outtextxy(100, 350, _T("r或R键->重新开始"));
	outtextxy(100, 400, _T("Esc键->返回主菜单"));


	outtextxy(100, 450, _T(""));
	outtextxy(100, 500, _T("得分说明:"));
	outtextxy(100, 550, _T("消除一行得一分"));
	// 绘制Pause按钮
	settextstyle(20, 0, _T("宋体"));
	setcolor(BLACK);
	const TCHAR* text = _T("返回");
	int textWidth = textwidth(text);
	int textHeight = textheight(text);
	outtextxy(backTextX1, backTextY1, text);
	backTextX2 = backTextX1 + textWidth;
	backTextY2 = backTextY1 + textHeight;
	rectangle(backTextX1 - 5, backTextY1 - 5, backTextX2 + 5, backTextY2 + 5);
}

// 分数显示
void showScoreInfo()
{
	_stprintf_s(currentScoreInfo, _T("当前得分: % 4d"), Score);
	outtextxy(pauseTextX1, 11 * StepSize, currentScoreInfo);
}

// 游戏时间显示
void showTimerInfo(DWORD timer) {
	_stprintf_s(currentScoreInfo, _T("游戏时间: % 4d"), timer);
	outtextxy(pauseTextX1, 12 * StepSize, currentScoreInfo);
}

void returnMenu() {
	showWelcomeUI();
}
//显示下一个方块样式
void NexTipDis()
{
	do
	{
		NextSty = rand() % 6;
	} while (CurrSty == NextSty);

	clearrectangle(StepSize * 11, StepSize * 3, StepSize * (11 + 4), StepSize * (3 + 2));
	for (int i = 0; i < 4; i++)
		putimage(StepSize * (Block[NextSty][i] % 4 + 11), StepSize * (Block[NextSty][i] / 4 + 3), &ImgBlo[NextSty]);

	FlushBatchDraw();
}

//产生新的方块
void ProdBlock()
{
	RotaOrig.X = 4;
	RotaOrig.Y = 1;
	CurrSty = NextSty;
	LongPresAble = false;
	Delay = 300;

	for (int i = 0; i < 4; i++)
		CurrBlo[i] = (Block[CurrSty][i] / 4) * 10 + (Block[CurrSty][i] % 4) + 3;

	NexTipDis();
}

// 方块下降动作
void Drop()
{
	//旋转点Y坐标加一
	RotaOrig.Y++;

	//下落之前备份
	for (int i = 0; i < 4; i++)
		Buff[i] = CurrBlo[i];


	for (int j = 0; j < 4; j++)
	{
		CurrBlo[j] += 10; // 将方块下落的格数改为10 * Speed

		//失败
		if (AllBlock[0][CurrBlo[j] % 10] != 0)
		{
			IsRun = false;
			return;
		}

		//非法 落地或重叠
		if (CurrBlo[j] / 10 == 20 || AllBlock[CurrBlo[j] / 10][CurrBlo[j] % 10] != 0)
		{
			for (int i = 0; i < 4; i++)
				AllBlock[Buff[i] / 10][Buff[i] % 10] = CurrSty + 1;

			ProdBlock();
			break;
		}
	}
}

void MoveX()
{
	char BeforeRow = 0;
	RefreSign = true;

	//旋转点X坐标向左或右移动一个单位
	RotaOrig.X += DirX;

	for (int i = 0; i < 4; i++)
		Buff[i] = CurrBlo[i];

	for (int j = 0; j < 4; j++)
	{
		BeforeRow = CurrBlo[j] / 10;

		CurrBlo[j] += DirX;

		//非法
		if (AllBlock[CurrBlo[j] / 10][CurrBlo[j] % 10] != 0 || BeforeRow != CurrBlo[j] / 10)
		{
			for (int i = 0; i < 4; i++)
				CurrBlo[i] = Buff[i];

			RotaOrig.X -= DirX;
			RefreSign = false;
			break;
		}
	}
	DirX = 0;
}

void CleanRow(char Row)
{
	showScoreInfo();
	for (int i = 0; i < Row; i++)
	{
		for (int j = 0; j < 10; j++)
			AllBlock[Row - i][j] = AllBlock[Row - i - 1][j];
	}
}

void Refresh()
{
	char BlockCount = 0;
	RefreSign = false;
	clearrectangle(0, 0, StepSize * COL, StepSize * ROW);

	for (int i = 0; i < 4; i++)
	{
		putimage(StepSize * (CurrBlo[i] % 10), StepSize * (CurrBlo[i] / 10), &ImgBlo[CurrSty]);
	}

	for (int r = 0; r < ROW; r++)
	{
		for (int c = 0; c < COL; c++)
		{
			if (AllBlock[r][c])
			{
				putimage(StepSize * c, StepSize * r, &ImgBlo[AllBlock[r][c] - 1]);

				if (COL == ++BlockCount)
				{
					Score++;
					CleanRow(r);
					BlockCount = 0;
				}
			}
		}
		BlockCount = 0;
	}
	FlushBatchDraw();
}

//方块 90°旋转处理
void Rotate()
{
	char Col = 0;
	char Row = 0;

	if (CurrSty == 4) return;

	for (int i = 0; i < 4; i++)
	{
		Col = RotaOrig.X + ((CurrBlo[i] / 10) - RotaOrig.Y);
		Row = RotaOrig.Y + (RotaOrig.X - (CurrBlo[i] % 10));

		//非法
		if (AllBlock[Row][Col] != 0 || Col < 0 || Col >= 10)  return;

		//合法
		Buff[i] = Row * 10 + Col;
	}

	//合法
	RefreSign = true;
	for (int j = 0; j < 4; j++)
		CurrBlo[j] = Buff[j];
}

//消息处理
void processMessage()
{
	if (peekmessage(&msg, EM_KEY))
	{
		if (msg.prevdown != 1)
		{
			switch (msg.vkcode)
			{
			case VK_LEFT: DirX = -1; break;
			case VK_RIGHT: DirX = 1; break;
			case VK_UP: Rotate(); break;
			case VK_DOWN: Delay = 70; break;
			}
			LongPresAble = true;
		}
		else if (msg.vkcode == VK_DOWN && LongPresAble == true) Delay = 70;
	}
}


void clearAllBlock()
{
	char BlockCount = 0;
	RefreSign = false;
	clearrectangle(0, 0, StepSize * COL, StepSize * ROW);

	// 将所有的当前方块绘制到屏幕上
	for (int i = 0; i < 4; i++)
	{
		putimage(StepSize * (CurrBlo[i] % 10), StepSize * (CurrBlo[i] / 10), &ImgBlo[CurrSty]);
	}

	// 遍历所有方块，将它们绘制到屏幕上
	for (int r = 0; r < ROW; r++)
	{
		for (int c = 0; c < COL; c++)
		{
			if (AllBlock[r][c])
			{
				putimage(StepSize * c, StepSize * r, &ImgBlo[AllBlock[r][c] - 1]);

				// 检查是否有满行
				if (COL == ++BlockCount)
				{
					Score++;
					CleanRow(r);
					BlockCount = 0;
				}
			}
		}
		BlockCount = 0;
	}

	// 将所有方块的值都设置为 0，即清空所有方块
	memset(AllBlock, 0, sizeof(AllBlock));

	// 更新得分信息，并刷新屏幕
	showScoreInfo();
	FlushBatchDraw();
}
