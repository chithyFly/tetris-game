#ifndef TETRISGAME_H
#define TETRISGAME_H
#include <graphics.h>

#define BUTTON_WIDTH 200
#define BUTTON_HEIGHT 

const char ROW = 20;
const char COL = 10;
const char StepSize = 30;
const char NexTipRow = 2;
const char NexTipCol = 11;



void runGame();
void playGame();

void showWelcomeUI();
void showGameUI();
void showGameSettingsUI();
void showHelpUI();
void showScoreInfo();
void showTimerInfo(DWORD timer);

void drawButtonAndText(const TCHAR* text, int x1, int y1, int height);
void drawBackground();

void Refresh();
void processMessage();
void Rotate();
void CleanRow(char Row);
void MoveX();
void Drop();
void ProdBlock();
void NexTipDis();

void clearAllBlock();
void showTimerInfo(DWORD timer);
#endif // TETRISGAME_H
